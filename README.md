
# Todo List Project

Hello! Welcome to my Todo List project. This is my first time working with JavaScript , and I'm still learning the ropes of coding. I've created a simple website where you can list your tasks. It's designed to help you stay organized and get things done. You can add tasks, edit them, and remove them easily. I'm still working on a few things, like the check mark button and updating the time, but I'll be adding those features soon. 

## Features

- **Easy to Use:** The website is simple to understand and use.
- **Organize Tasks:** You can sort your tasks into categories so it's easier to manage them.
- **Duplicate Prevention:** The website is smart and won't let you add the same task more than once, even if you edit it.
- **Dynamic Ad Display:** You'll see ads on the website. They change when you have 5, 4, or 3 tasks, and after that, you'll see an ad after every task. The ad will show the most popular video from Aparat.
- **Responsive Design:** Works seamlessly on both desktop and mobile devices.

### How to Get Started

1. Clone the repository to your local machine:
2. Open the file called `index.html` in your web browser.

## How to Use

1. Open the website in your web browser.
2. Press the ">>" icon to create a new task. You can also press the Enter key to add a task.
3. You can mark a task as done by clicking the checkbox (I'm still working on this feature).
4. To change a task, click the "Edit" button and make your changes. The website won't let you add the same task again by mistake.
5. If you want to get rid of a task, click the "Delete" button.

When you have 5, 4, or 3 tasks, you'll see an ad. After that, there will be an ad after each task you add. The ad will show the most popular video from Aparat.

## How to Help

I'd be happy if you want to help! If you're good at coding, you can suggest changes or fix any issues you find. Here's how:

1. Make a copy of the code on your own GitLab account.
2. Create a new part of the code for the thing you want to add or change.
3. Make your changes and save them.
4. Send your changes to me by clicking the "Pull Request" button on GitLab.

## Questions or Ideas?

If you have any questions or ideas, feel free to send me an email at [sabasrji@gmail.com](mailto:sabasrji.email@gmail.com).

---

Thanks for using my Todo List website! I hope it helps you stay organized and get things done. If you find any problems or have suggestions, please let me know. I'll be adding more features soon, so keep an eye out!

