"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) { return typeof obj; } : function (obj) { return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }, _typeof(obj); }
function _regeneratorRuntime() { "use strict"; /*! regenerator-runtime -- Copyright (c) 2014-present, Facebook, Inc. -- license (MIT): https://github.com/facebook/regenerator/blob/main/LICENSE */ _regeneratorRuntime = function _regeneratorRuntime() { return exports; }; var exports = {}, Op = Object.prototype, hasOwn = Op.hasOwnProperty, defineProperty = Object.defineProperty || function (obj, key, desc) { obj[key] = desc.value; }, $Symbol = "function" == typeof Symbol ? Symbol : {}, iteratorSymbol = $Symbol.iterator || "@@iterator", asyncIteratorSymbol = $Symbol.asyncIterator || "@@asyncIterator", toStringTagSymbol = $Symbol.toStringTag || "@@toStringTag"; function define(obj, key, value) { return Object.defineProperty(obj, key, { value: value, enumerable: !0, configurable: !0, writable: !0 }), obj[key]; } try { define({}, ""); } catch (err) { define = function define(obj, key, value) { return obj[key] = value; }; } function wrap(innerFn, outerFn, self, tryLocsList) { var protoGenerator = outerFn && outerFn.prototype instanceof Generator ? outerFn : Generator, generator = Object.create(protoGenerator.prototype), context = new Context(tryLocsList || []); return defineProperty(generator, "_invoke", { value: makeInvokeMethod(innerFn, self, context) }), generator; } function tryCatch(fn, obj, arg) { try { return { type: "normal", arg: fn.call(obj, arg) }; } catch (err) { return { type: "throw", arg: err }; } } exports.wrap = wrap; var ContinueSentinel = {}; function Generator() {} function GeneratorFunction() {} function GeneratorFunctionPrototype() {} var IteratorPrototype = {}; define(IteratorPrototype, iteratorSymbol, function () { return this; }); var getProto = Object.getPrototypeOf, NativeIteratorPrototype = getProto && getProto(getProto(values([]))); NativeIteratorPrototype && NativeIteratorPrototype !== Op && hasOwn.call(NativeIteratorPrototype, iteratorSymbol) && (IteratorPrototype = NativeIteratorPrototype); var Gp = GeneratorFunctionPrototype.prototype = Generator.prototype = Object.create(IteratorPrototype); function defineIteratorMethods(prototype) { ["next", "throw", "return"].forEach(function (method) { define(prototype, method, function (arg) { return this._invoke(method, arg); }); }); } function AsyncIterator(generator, PromiseImpl) { function invoke(method, arg, resolve, reject) { var record = tryCatch(generator[method], generator, arg); if ("throw" !== record.type) { var result = record.arg, value = result.value; return value && "object" == _typeof(value) && hasOwn.call(value, "__await") ? PromiseImpl.resolve(value.__await).then(function (value) { invoke("next", value, resolve, reject); }, function (err) { invoke("throw", err, resolve, reject); }) : PromiseImpl.resolve(value).then(function (unwrapped) { result.value = unwrapped, resolve(result); }, function (error) { return invoke("throw", error, resolve, reject); }); } reject(record.arg); } var previousPromise; defineProperty(this, "_invoke", { value: function value(method, arg) { function callInvokeWithMethodAndArg() { return new PromiseImpl(function (resolve, reject) { invoke(method, arg, resolve, reject); }); } return previousPromise = previousPromise ? previousPromise.then(callInvokeWithMethodAndArg, callInvokeWithMethodAndArg) : callInvokeWithMethodAndArg(); } }); } function makeInvokeMethod(innerFn, self, context) { var state = "suspendedStart"; return function (method, arg) { if ("executing" === state) throw new Error("Generator is already running"); if ("completed" === state) { if ("throw" === method) throw arg; return { value: void 0, done: !0 }; } for (context.method = method, context.arg = arg;;) { var delegate = context.delegate; if (delegate) { var delegateResult = maybeInvokeDelegate(delegate, context); if (delegateResult) { if (delegateResult === ContinueSentinel) continue; return delegateResult; } } if ("next" === context.method) context.sent = context._sent = context.arg;else if ("throw" === context.method) { if ("suspendedStart" === state) throw state = "completed", context.arg; context.dispatchException(context.arg); } else "return" === context.method && context.abrupt("return", context.arg); state = "executing"; var record = tryCatch(innerFn, self, context); if ("normal" === record.type) { if (state = context.done ? "completed" : "suspendedYield", record.arg === ContinueSentinel) continue; return { value: record.arg, done: context.done }; } "throw" === record.type && (state = "completed", context.method = "throw", context.arg = record.arg); } }; } function maybeInvokeDelegate(delegate, context) { var methodName = context.method, method = delegate.iterator[methodName]; if (undefined === method) return context.delegate = null, "throw" === methodName && delegate.iterator.return && (context.method = "return", context.arg = undefined, maybeInvokeDelegate(delegate, context), "throw" === context.method) || "return" !== methodName && (context.method = "throw", context.arg = new TypeError("The iterator does not provide a '" + methodName + "' method")), ContinueSentinel; var record = tryCatch(method, delegate.iterator, context.arg); if ("throw" === record.type) return context.method = "throw", context.arg = record.arg, context.delegate = null, ContinueSentinel; var info = record.arg; return info ? info.done ? (context[delegate.resultName] = info.value, context.next = delegate.nextLoc, "return" !== context.method && (context.method = "next", context.arg = undefined), context.delegate = null, ContinueSentinel) : info : (context.method = "throw", context.arg = new TypeError("iterator result is not an object"), context.delegate = null, ContinueSentinel); } function pushTryEntry(locs) { var entry = { tryLoc: locs[0] }; 1 in locs && (entry.catchLoc = locs[1]), 2 in locs && (entry.finallyLoc = locs[2], entry.afterLoc = locs[3]), this.tryEntries.push(entry); } function resetTryEntry(entry) { var record = entry.completion || {}; record.type = "normal", delete record.arg, entry.completion = record; } function Context(tryLocsList) { this.tryEntries = [{ tryLoc: "root" }], tryLocsList.forEach(pushTryEntry, this), this.reset(!0); } function values(iterable) { if (iterable || "" === iterable) { var iteratorMethod = iterable[iteratorSymbol]; if (iteratorMethod) return iteratorMethod.call(iterable); if ("function" == typeof iterable.next) return iterable; if (!isNaN(iterable.length)) { var i = -1, next = function next() { for (; ++i < iterable.length;) if (hasOwn.call(iterable, i)) return next.value = iterable[i], next.done = !1, next; return next.value = undefined, next.done = !0, next; }; return next.next = next; } } throw new TypeError(_typeof(iterable) + " is not iterable"); } return GeneratorFunction.prototype = GeneratorFunctionPrototype, defineProperty(Gp, "constructor", { value: GeneratorFunctionPrototype, configurable: !0 }), defineProperty(GeneratorFunctionPrototype, "constructor", { value: GeneratorFunction, configurable: !0 }), GeneratorFunction.displayName = define(GeneratorFunctionPrototype, toStringTagSymbol, "GeneratorFunction"), exports.isGeneratorFunction = function (genFun) { var ctor = "function" == typeof genFun && genFun.constructor; return !!ctor && (ctor === GeneratorFunction || "GeneratorFunction" === (ctor.displayName || ctor.name)); }, exports.mark = function (genFun) { return Object.setPrototypeOf ? Object.setPrototypeOf(genFun, GeneratorFunctionPrototype) : (genFun.__proto__ = GeneratorFunctionPrototype, define(genFun, toStringTagSymbol, "GeneratorFunction")), genFun.prototype = Object.create(Gp), genFun; }, exports.awrap = function (arg) { return { __await: arg }; }, defineIteratorMethods(AsyncIterator.prototype), define(AsyncIterator.prototype, asyncIteratorSymbol, function () { return this; }), exports.AsyncIterator = AsyncIterator, exports.async = function (innerFn, outerFn, self, tryLocsList, PromiseImpl) { void 0 === PromiseImpl && (PromiseImpl = Promise); var iter = new AsyncIterator(wrap(innerFn, outerFn, self, tryLocsList), PromiseImpl); return exports.isGeneratorFunction(outerFn) ? iter : iter.next().then(function (result) { return result.done ? result.value : iter.next(); }); }, defineIteratorMethods(Gp), define(Gp, toStringTagSymbol, "Generator"), define(Gp, iteratorSymbol, function () { return this; }), define(Gp, "toString", function () { return "[object Generator]"; }), exports.keys = function (val) { var object = Object(val), keys = []; for (var key in object) keys.push(key); return keys.reverse(), function next() { for (; keys.length;) { var key = keys.pop(); if (key in object) return next.value = key, next.done = !1, next; } return next.done = !0, next; }; }, exports.values = values, Context.prototype = { constructor: Context, reset: function reset(skipTempReset) { if (this.prev = 0, this.next = 0, this.sent = this._sent = undefined, this.done = !1, this.delegate = null, this.method = "next", this.arg = undefined, this.tryEntries.forEach(resetTryEntry), !skipTempReset) for (var name in this) "t" === name.charAt(0) && hasOwn.call(this, name) && !isNaN(+name.slice(1)) && (this[name] = undefined); }, stop: function stop() { this.done = !0; var rootRecord = this.tryEntries[0].completion; if ("throw" === rootRecord.type) throw rootRecord.arg; return this.rval; }, dispatchException: function dispatchException(exception) { if (this.done) throw exception; var context = this; function handle(loc, caught) { return record.type = "throw", record.arg = exception, context.next = loc, caught && (context.method = "next", context.arg = undefined), !!caught; } for (var i = this.tryEntries.length - 1; i >= 0; --i) { var entry = this.tryEntries[i], record = entry.completion; if ("root" === entry.tryLoc) return handle("end"); if (entry.tryLoc <= this.prev) { var hasCatch = hasOwn.call(entry, "catchLoc"), hasFinally = hasOwn.call(entry, "finallyLoc"); if (hasCatch && hasFinally) { if (this.prev < entry.catchLoc) return handle(entry.catchLoc, !0); if (this.prev < entry.finallyLoc) return handle(entry.finallyLoc); } else if (hasCatch) { if (this.prev < entry.catchLoc) return handle(entry.catchLoc, !0); } else { if (!hasFinally) throw new Error("try statement without catch or finally"); if (this.prev < entry.finallyLoc) return handle(entry.finallyLoc); } } } }, abrupt: function abrupt(type, arg) { for (var i = this.tryEntries.length - 1; i >= 0; --i) { var entry = this.tryEntries[i]; if (entry.tryLoc <= this.prev && hasOwn.call(entry, "finallyLoc") && this.prev < entry.finallyLoc) { var finallyEntry = entry; break; } } finallyEntry && ("break" === type || "continue" === type) && finallyEntry.tryLoc <= arg && arg <= finallyEntry.finallyLoc && (finallyEntry = null); var record = finallyEntry ? finallyEntry.completion : {}; return record.type = type, record.arg = arg, finallyEntry ? (this.method = "next", this.next = finallyEntry.finallyLoc, ContinueSentinel) : this.complete(record); }, complete: function complete(record, afterLoc) { if ("throw" === record.type) throw record.arg; return "break" === record.type || "continue" === record.type ? this.next = record.arg : "return" === record.type ? (this.rval = this.arg = record.arg, this.method = "return", this.next = "end") : "normal" === record.type && afterLoc && (this.next = afterLoc), ContinueSentinel; }, finish: function finish(finallyLoc) { for (var i = this.tryEntries.length - 1; i >= 0; --i) { var entry = this.tryEntries[i]; if (entry.finallyLoc === finallyLoc) return this.complete(entry.completion, entry.afterLoc), resetTryEntry(entry), ContinueSentinel; } }, catch: function _catch(tryLoc) { for (var i = this.tryEntries.length - 1; i >= 0; --i) { var entry = this.tryEntries[i]; if (entry.tryLoc === tryLoc) { var record = entry.completion; if ("throw" === record.type) { var thrown = record.arg; resetTryEntry(entry); } return thrown; } } throw new Error("illegal catch attempt"); }, delegateYield: function delegateYield(iterable, resultName, nextLoc) { return this.delegate = { iterator: values(iterable), resultName: resultName, nextLoc: nextLoc }, "next" === this.method && (this.arg = undefined), ContinueSentinel; } }, exports; }
function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }
function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }
function _createForOfIteratorHelper(o, allowArrayLike) { var it = typeof Symbol !== "undefined" && o[Symbol.iterator] || o["@@iterator"]; if (!it) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = it.call(o); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it.return != null) it.return(); } finally { if (didErr) throw err; } } }; }
function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }
function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) arr2[i] = arr[i]; return arr2; }
var ul = document.querySelector("ul");
var input = document.getElementById("myInput");
var addBtn = document.getElementById("addBtn");
setAddedCount = function setAddedCount(value) {
  return localStorage.setItem("addedCount", value.toString());
};
handlerAddTodo = function handlerAddTodo(todoArr, object, countNum) {
  input.value = "";
  todoArr.push(object);
  setLocal(todoArr);
  generteTodo(todoArr);
  ads(todoArr);
  countNum++;
  setAddedCount(countNum);
  input.focus();
};
addNewTodo = function addNewTodo() {
  var inputValue = input.value;
  var counted = Number(localStorage.getItem("addedCount"));
  var newTodoArr = JSON.parse(localStorage.getItem("todos")) || [];
  if (dublicateTask(inputValue) !== -1) {
    alert("dublicate :(");
    input.value = "";
  } else {
    var todoObj = {
      id: newTodoArr.length + 1,
      title: inputValue
    };
    if (todoObj.title === "") {
      alert("empty :)");
      return;
    } else {
      input.value = "";
      switch (true) {
        case counted > 1 && counted === 5:
        case counted > 6 && counted === 9:
        case counted > 9 && counted === 12:
        case counted > 13 && counted === 14:
        case counted > 14:
          return handlerAddTodo(newTodoArr, todoObj, counted);
        default:
          newTodoArr.push(todoObj);
          setLocal(newTodoArr);
          generteTodo(newTodoArr);
      }
      counted++;
      setAddedCount(counted);
    }
  }
};
setLocal = function setLocal(todolist) {
  return localStorage.setItem("todos", JSON.stringify(todolist));
};
var getLocal = function getLocal() {
  var localStorageTodos = JSON.parse(localStorage.getItem("todos"));
  if (localStorageTodos) {
    todoArr = localStorageTodos;
  } else {
    todoArr = [];
  }
  generteTodo(todoArr);
};
window.addEventListener("load", getLocal);
generteTodo = function generteTodo(todolist) {
  input.focus();
  ul.innerHTML = "";
  todolist.forEach(function (todo) {
    var li = document.createElement("li");
    li.className = "box";
    var wrapper = document.createElement("div");
    wrapper.className = "d-flex align-items-center";
    var markBtn = document.createElement("button");
    markBtn.className = "checkbox";
    markBtn.innerHTML = "&check;";
    var p = document.createElement("p");
    p.innerHTML = todo.title;
    var root = document.createElement("div");
    root.className = "d-flex";
    var timeWrapper = document.createElement("div");
    timeWrapper.className = "data";
    timeWrapper.innerHTML = "<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"15\" height=\"15\" fill=\"currentColor\" class=\"bi bi-clock me-2\" viewBox=\"0 0 16 16\">\n    <path d=\"M8 3.5a.5.5 0 0 0-1 0V9a.5.5 0 0 0 .252.434l3.5 2a.5.5 0 0 0 .496-.868L8 8.71V3.5z\"/>\n    <path d=\"M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm7-8A7 7 0 1 1 1 8a7 7 0 0 1 14 0z\"/>\n    </svg>";
    var sapnTime = document.createElement("span");
    var editBtn = document.createElement("button");
    editBtn.className = "rounded-1 border-0 text-white mx-3 bg-grip";
    editBtn.innerHTML = "Edit";
    editBtn.setAttribute("onclick", "editTask(" + todo.id + ")");
    var dltBtn = document.createElement("button");
    dltBtn.className = "delete-btn";
    dltBtn.innerHTML = "&times;";
    dltBtn.setAttribute("onclick", "removeTodo(" + todo.id + ")");
    ul.appendChild(li), li.appendChild(wrapper), wrapper.appendChild(markBtn), wrapper.appendChild(p), li.appendChild(root), root.appendChild(timeWrapper), timeWrapper.appendChild(sapnTime), root.appendChild(editBtn), root.appendChild(dltBtn);
  });

  // });
};

addBtn.addEventListener("click", function () {
  return addNewTodo(todoArr);
});
input.addEventListener("keydown", function (event) {
  if (event.code === "Enter") {
    addNewTodo();
  }
});
removeTodo = function removeTodo(todoId) {
  var localStorageTodos = JSON.parse(localStorage.getItem("todos"));
  var todoIndex = localStorageTodos.findIndex(function (todo) {
    return todo.id === todoId;
  });
  localStorageTodos.splice(todoIndex, 1);
  setLocal(localStorageTodos);
  generteTodo(localStorageTodos);
};
editTask = function editTask(todoId) {
  var saveBtn = document.getElementById("saveBtn");
  saveBtn.setAttribute("taskId", todoId);
  var modalParent = document.getElementsByClassName("modal-parent");
  var editInput = document.getElementById("inputEdit");
  var closeElm = document.querySelector(".X");
  closeElm.addEventListener("click", function () {
    return modalParent[0].style.display = "none";
  });
  modalParent[0].style.display = "block";
  var localStorageTodos = JSON.parse(localStorage.getItem("todos"));
  var todoIndex = localStorageTodos.findIndex(function (todo) {
    return todo.id === todoId;
  });
  editTitle = localStorageTodos[todoIndex].title;
  editInput.value = editTitle;
  editInput.focus();
};
saveBtn.addEventListener("click", function (e) {
  var target = e.target.getAttribute("taskId");
  var modalParent = document.getElementsByClassName("modal-parent");
  var editInput = document.getElementById("inputEdit");
  modalParent[0].style.display = "none";
  console.log("######", target, editInput.value);
  setNewValue(editInput.value, target);
});
setNewValue = function setNewValue(editInput, id) {
  var localStorageTodos = JSON.parse(localStorage.getItem("todos"));
  var todoIndex = localStorageTodos.findIndex(function (todo) {
    return todo.id === +id;
  });
  var _iterator = _createForOfIteratorHelper(localStorageTodos),
    _step;
  try {
    for (_iterator.s(); !(_step = _iterator.n()).done;) {
      var todo = _step.value;
      if (todo.title === editInput) {
        alert("Already available :(");
        return;
      }
    }
  } catch (err) {
    _iterator.e(err);
  } finally {
    _iterator.f();
  }
  localStorageTodos[todoIndex].title = editInput;
  setLocal(localStorageTodos);
  generteTodo(localStorageTodos);
};
var dublicateTask = function dublicateTask(value) {
  var localStorageTodos = JSON.parse(localStorage.getItem("todos"));
  if (!localStorageTodos) {
    return -1;
  }
  return localStorageTodos.findIndex(function (item) {
    return item.title === value;
  });
};
function ads() {
  return _ads.apply(this, arguments);
}
function _ads() {
  _ads = _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee() {
    var response, data, mostViewedVideos;
    return _regeneratorRuntime().wrap(function _callee$(_context) {
      while (1) switch (_context.prev = _context.next) {
        case 0:
          _context.prev = 0;
          _context.next = 3;
          return fetch("http://api.aparat.com/fa/v1/video/video/mostViewedVideos");
        case 3:
          response = _context.sent;
          _context.next = 6;
          return response.json();
        case 6:
          data = _context.sent;
          mostViewedVideos = mostView(data);
          modalVideo(mostViewedVideos);
          _context.next = 14;
          break;
        case 11:
          _context.prev = 11;
          _context.t0 = _context["catch"](0);
          alert("ERROE");
        case 14:
        case "end":
          return _context.stop();
      }
    }, _callee, null, [[0, 11]]);
  }));
  return _ads.apply(this, arguments);
}
mostView = function mostView(data) {
  var dataInfo = data.data;
  var arrNum = [];
  for (var i = 0; i < dataInfo.length; i++) {
    var visitCnt = Number(dataInfo[i].attributes.visit_cnt);
    arrNum.push(visitCnt);
  }
  var indexMaxNum = dataInfo.findIndex(function (item) {
    var mathMax = Math.max.apply(Math, arrNum);
    return Number(item.attributes.visit_cnt) === mathMax;
  });
  return dataInfo[indexMaxNum];
};
modalVideo = function modalVideo(video) {
  var videoTitle = video.attributes.title;
  var videoFrame = video.attributes.frame;
  var modalParent = document.getElementById("modalParent");
  modalParent.style.display = "block";
  var modalTitle = document.getElementById("title");
  modalTitle.innerHTML = videoTitle;
  var iframe = document.createElement("iframe");
  iframe.setAttribute("allowFullScreen", "true");
  iframe.setAttribute("src", videoFrame);
  iframe.className = "w-100 h-100";
  var modalElm = document.getElementById("modalElm");
  modalElm.appendChild(iframe);
  var closeElm = document.getElementById("closeElm");
  closeElm.addEventListener("click", function () {
    modalParent.style.display = "none";
    iframe.remove();
  });
};
