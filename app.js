let ul = document.querySelector("ul");
let input = document.getElementById("myInput");
let addBtn = document.getElementById("addBtn");

setAddedCount = (value) => localStorage.setItem("addedCount", value.toString());

handlerAddTodo = (todoArr, object, countNum) => {
  input.value = "";
  todoArr.push(object);
  setLocal(todoArr);
  generteTodo(todoArr);
  ads(todoArr);
  countNum++;
  setAddedCount(countNum);
  input.focus();
};

addNewTodo = () => {
  let inputValue = input.value;

  let counted = Number(localStorage.getItem("addedCount"));

  const newTodoArr = JSON.parse(localStorage.getItem("todos")) || [];

  if (dublicateTask(inputValue) !== -1) {

    alert("dublicate :(");
    input.value = "";
  } else {
    let todoObj = {
      id: newTodoArr.length + 1,
      title: inputValue,
    };

    if (todoObj.title === "") {
      alert("empty :)");
      return;
    } else {
      input.value = "";

      switch (true) {
        case counted > 1 && counted === 5:
        case counted > 6 && counted === 9:
        case counted > 9 && counted === 12:
        case counted > 13 && counted === 14:
        case counted > 14:
          return handlerAddTodo(newTodoArr, todoObj, counted);

        default:
          newTodoArr.push(todoObj);
          setLocal(newTodoArr);
          generteTodo(newTodoArr);
      }
      counted++;
      setAddedCount(counted);
    }
  }
};

setLocal = (todolist) =>
  localStorage.setItem("todos", JSON.stringify(todolist));

let getLocal = () => {
  let localStorageTodos = JSON.parse(localStorage.getItem("todos"));

  if (localStorageTodos) {
    todoArr = localStorageTodos;
  } else {
    todoArr = [];
  }
  generteTodo(todoArr);
};
window.addEventListener("load", getLocal);

generteTodo = (todolist) => {
  input.focus();
  ul.innerHTML = "";

  todolist.forEach((todo) => {
    const li = document.createElement("li");
    li.className = "box";

    const wrapper = document.createElement("div");
    wrapper.className = "d-flex align-items-center";

    const markBtn = document.createElement("button");
    markBtn.className = "checkbox";
    markBtn.innerHTML = "&check;";

    const p = document.createElement("p");
    p.innerHTML = todo.title;

    const root = document.createElement("div");
    root.className = "d-flex";

    const timeWrapper = document.createElement("div");
    timeWrapper.className = "data";
    timeWrapper.innerHTML = `<svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" fill="currentColor" class="bi bi-clock me-2" viewBox="0 0 16 16">
    <path d="M8 3.5a.5.5 0 0 0-1 0V9a.5.5 0 0 0 .252.434l3.5 2a.5.5 0 0 0 .496-.868L8 8.71V3.5z"/>
    <path d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm7-8A7 7 0 1 1 1 8a7 7 0 0 1 14 0z"/>
    </svg>`;

    const sapnTime = document.createElement("span");

    const editBtn = document.createElement("button");
    editBtn.className = "rounded-1 border-0 text-white mx-3 bg-grip";
    editBtn.innerHTML = "EDIT";
    editBtn.setAttribute("onclick", "editTask(" + todo.id + ")");

    const dltBtn = document.createElement("button");
    dltBtn.className = "delete-btn";
    dltBtn.innerHTML = "&times;";
    dltBtn.setAttribute("onclick", "removeTodo(" + todo.id + ")");

    ul.appendChild(li),
      li.appendChild(wrapper),
      wrapper.appendChild(markBtn),
      wrapper.appendChild(p),
      li.appendChild(root),
      root.appendChild(timeWrapper),
      timeWrapper.appendChild(sapnTime),
      root.appendChild(editBtn),
      root.appendChild(dltBtn);
  });

  // });
};

addBtn.addEventListener("click", () => addNewTodo(todoArr));

input.addEventListener("keydown", (event) => {
  if (event.code === "Enter") {
    addNewTodo();
  }
});

removeTodo = (todoId) => {
  let localStorageTodos = JSON.parse(localStorage.getItem("todos"));

  let todoIndex = localStorageTodos.findIndex((todo) => todo.id === todoId);

  localStorageTodos.splice(todoIndex, 1);

  setLocal(localStorageTodos);
  generteTodo(localStorageTodos);
};

editTask = (todoId) => {
 
  let saveBtn = document.getElementById("saveBtn");
  saveBtn.setAttribute("taskId", todoId);

  let modalParent = document.getElementsByClassName("modal-parent");
  let editInput = document.getElementById("inputEdit");

  let closeElm = document.querySelector(".X");
  closeElm.addEventListener(
    "click",
    () => (modalParent[0].style.display = "none")
  );

  modalParent[0].style.display = "block";

  let localStorageTodos = JSON.parse(localStorage.getItem("todos"));

  let todoIndex = localStorageTodos.findIndex((todo) => todo.id === todoId);

  editTitle = localStorageTodos[todoIndex].title;
  editInput.value = editTitle;

  editInput.focus();
};

saveBtn.addEventListener("click", (e) => {
  const target = e.target.getAttribute("taskId");
  let modalParent = document.getElementsByClassName("modal-parent");
  let editInput = document.getElementById("inputEdit");

  modalParent[0].style.display = "none";

  console.log("######", target, editInput.value);
  setNewValue(editInput.value, target);
});

setNewValue = (editInput, id) => {
  let localStorageTodos = JSON.parse(localStorage.getItem("todos"));

  let todoIndex = localStorageTodos.findIndex((todo) => todo.id === +id);


    for (const todo of localStorageTodos) {
      if (todo.title === editInput) {
          alert("Already available :(");
          return;
      }
  }


  localStorageTodos[todoIndex].title = editInput;
  setLocal(localStorageTodos);
  generteTodo(localStorageTodos);
};

const dublicateTask = (value) => {
  let localStorageTodos = JSON.parse(localStorage.getItem("todos"));

  if (!localStorageTodos) {
    return -1;
  }

   return localStorageTodos.findIndex((item) => item.title === value);

};


async function ads() {
    try{

    const response = await fetch("http://api.aparat.com/fa/v1/video/video/mostViewedVideos")
    const data = await response.json();

    let mostViewedVideos = mostView(data)
        modalVideo(mostViewedVideos) 

    }catch{
        alert("ERROE")
    }
  
}


mostView = (data) => {
  const dataInfo = data.data;
  const arrNum = [];

  for (let i = 0; i < dataInfo.length; i++) {
    let visitCnt = Number(dataInfo[i].attributes.visit_cnt);
    arrNum.push(visitCnt);
  }

  let indexMaxNum = dataInfo.findIndex((item) => {
    let mathMax = Math.max(...arrNum);

    return Number(item.attributes.visit_cnt) === mathMax;
  });
  return dataInfo[indexMaxNum];
};


modalVideo = (video) => {

  const videoTitle = video.attributes.title;
  const videoFrame = video.attributes.frame;

  let modalParent = document.getElementById("modalParent");
  modalParent.style.display = "block";

  const modalTitle = document.getElementById("title");
  modalTitle.innerHTML = videoTitle;

  const iframe = document.createElement("iframe");
  iframe.setAttribute("allowFullScreen", "true");
  iframe.setAttribute("src", videoFrame);
  iframe.className = "w-100 h-100";

  const modalElm = document.getElementById("modalElm");
  modalElm.appendChild(iframe);

  const closeElm = document.getElementById("closeElm");
  closeElm.addEventListener("click", () => {
    modalParent.style.display = "none";
    iframe.remove();
  });
};
